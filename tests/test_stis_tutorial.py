import os 
import glob

def test_stis_tutorial ( ) :
    directory = os.path.dirname ( os.path.abspath (__file__) ) + os.path.sep + os.path.join ('..', 'pyklip', 'instruments', 'stis_support', 'tutorial' )

    stisdatatargetdir = os.path.join ( directory, 'data', 'A10_427_VAUMIC_TRG' )
    targetfilelist = glob.glob ( stisdatatargetdir + os.path.sep + "*.fits" )
    assert ( len ( targetfilelist ) == 4 )

    stisdatareferencedir = os.path.join ( directory, 'data', 'A10_427_HD145570_REF' )
    referencefilelist = glob.glob ( stisdatareferencedir + os.path.sep + "*.fits" )
#    assert ( len ( referencefilelist ) == 8 ) # don't need this many
    assert ( len ( referencefilelist ) == 1 )

    outputdir = os.path.join ( directory, 'output' )

    maskdir = os.path.join ( directory, 'data', 'inputmasks' )
    maskdirfiles = os.listdir ( maskdir )
    for item in maskdirfiles :
        if ( item.endswith(".fits") ):
            fileMaskFQPN = maskdir + os.path.sep + item

    import pyklip.instruments.STIS as STIS
    import pyklip.parallelized as parallelized
    import pyklip.rdi as rdi

    dataset1 = STIS.STISData   (
                                trgSCIListrefSCIList    = [targetfilelist,referencefilelist] ,
                                outputFolder            = outputdir                ,
                                xExtent                 = 1044                     ,
                                yExtent                 = 427                      ,
                                IWA                     = 10                       ,
                                OWA                     = 200                      ,
                                yesApplyMaskFlag        = True                     ,
                                divertMaskFlag          = False                    ,
                                maskFilename            = fileMaskFQPN             ,
                                DatasetPlotFlag         = False                    ,
                                DQmax                   = pow ( 2, 14 )            ,
                                VL                      = 0                        ,
                                approvedFlag            = False                    ,
                                radonFlag               = True                     ,
                                userTestingNumberFrames = 2                        ,
                                #userTestingNumberFrames = 3                        ,
#                                userTestingNumberFrames = 0                        ,
#                                dynMask4ple             = ( True , 1 , 0 , 0 )     ,
                                dynMask4ple             = ( False , 0 , 0 , 0 )     ,
                                runWedgeDecided         = "A"                      ,
                                useRefandTrgFlag        = True                     ,
                                sizeMax                 = None                     ,
                                fileMaskWedgeCode       = 'A10'
                                )

    psflib = rdi.PSFLibrary (
                             dataset1.input              , # psflib_imgs
                             dataset1.centers[0]         , # aligned_center,
                             dataset1.filenames          , # psflib_filenames,
                             compute_correlation = True    # "create new corr_matrix"
                             )
    # Get the instance where the frames switch from target frames to reference frames...
    for tvrListIndex in range ( len ( dataset1.tvrList ) ) :
        if dataset1.tvrList [ tvrListIndex ] != 0:
            break

    # Delete the reference frames, leaving only the target frames
    dataset1.filenames = dataset1.filenames[:tvrListIndex]
    dataset1._centers  = dataset1._centers[:tvrListIndex]
    dataset1._input    = dataset1._input[:tvrListIndex]
    dataset1._PAs      = dataset1._PAs[:tvrListIndex]
    dataset1._wcs      = dataset1._wcs[:tvrListIndex]
    dataset1._wvs      = dataset1._wvs[:tvrListIndex]
    dataset1._filenums = dataset1._filenums[:tvrListIndex]
    dataset1.tvrList   = dataset1.tvrList[:tvrListIndex]
    dataset1._error    = dataset1._error[:tvrListIndex]

    psflib.prepare_library ( dataset1 )

    parallelized.klip_dataset   (
                                 dataset1                             ,
                                 outputdir      = outputdir           ,
                                 fileprefix     = ""                  ,  
                                 annuli         = 1                   , # recommended for disk
                                 subsections    = 1                   , # recommended for disk
                                 numbasis       = [ 1, 5, 10 ]        ,
                                 maxnumbasis    = 150                 ,
                                 mode           = "RDI"               ,
                                 aligned_center = dataset1.centers[0] ,
                                 psf_library    = psflib              ,
                                 movement       = 1
                                 )


if __name__ == "__main__" : 
    test_stis_tutorial()
